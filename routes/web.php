<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(url('profile', ['user_name' => auth()->user()->user_name]));
})->middleware('auth');

Route::get('/profile/{user_name}', 'ProfileController@index')->middleware('auth');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'Auth\RegisterController@getRegister')->name('register');



/**
 * JSON End point
 */
require __dir__ . '/jsonend.php';





/**
 * Testing middleware
 */

Route::get('view/test', function () {
  return json_encode(auth()->user()->isStudent());
});

Route::get('/view/student', function () {
  return 'only student can view student page';
})->middleware('student');

Route::get('/view/admin', function () {
  return 'only admin can view admin page';
})->middleware('admin');

Route::get('/view/school', function () {
  return 'only school can view school page';
})->middleware('school');

Route::get('/view/tutor', function () {
  return 'only tutor can view tutor page';
})->middleware('tutor');

Route::get('/view/counselor', function () {
  return 'only counselor can view counselor page';
})->middleware('counselor');

Route::get('/view/parent', function () {
  return 'only parent can view parent page';
})->middleware('parent');






