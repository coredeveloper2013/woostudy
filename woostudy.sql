-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 07:22 PM
-- Server version: 5.7.20
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `woostudy`
--

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(10) UNSIGNED NOT NULL,
  `grade_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institute` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `percentage_obtained` int(11) DEFAULT NULL,
  `education_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `grade_name`, `user_id`, `grade`, `institute`, `completion_date`, `percentage_obtained`, `education_level`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Sammy Gerhold', 4, 'quia', 'modi', '1971-09-09', 50, 'et', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(2, 'Miss Juana Zemlak', 1, 'eos', 'maxime', '1992-07-27', 63, 'porro', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 'Adela Dibbert DVM', 1, 'veritatis', 'ad', '1982-07-26', 54, 'laborum', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 'Arnold Schoen', 4, 'qui', 'qui', '1970-12-03', 86, 'nulla', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(5, 'Nicholaus Armstrong', 2, 'omnis', 'eveniet', '2002-06-15', 82, 'eos', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(6, 'Dr. Terry Bogisich', 2, 'labore', 'iure', '1976-03-17', 63, 'architecto', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(7, 'Wade Carroll III', 4, 'illo', 'vel', '1976-10-09', 54, 'ea', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(8, 'Jaylin Schaefer', 1, 'eum', 'nihil', '1973-08-02', 56, 'qui', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(9, 'Joelle Herzog', 1, 'voluptatibus', 'vel', '1993-04-26', 87, 'et', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(10, 'Dr. Emiliano Haag V', 3, 'fugiat', 'maiores', '2005-05-20', 64, 'et', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(11, 'Ms. Jennifer Walter', 1, 'velit', 'iure', '2006-07-31', 63, 'qui', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(12, 'Oral Lebsack', 4, 'voluptatem', 'facere', '1982-09-19', 83, 'est', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(13, 'Duane Greenholt', 3, 'qui', 'ex', '1997-08-23', 70, 'quia', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(14, 'Ms. Laurianne Aufderhar DDS', 3, 'qui', 'atque', '1999-08-01', 85, 'dignissimos', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(15, 'Tyree Morissette', 2, 'ut', 'blanditiis', '1999-04-04', 85, 'quasi', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(16, 'Mr. Danial Wehner', 1, 'vel', 'eos', '2017-09-06', 58, 'quisquam', '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `ielts`
--

CREATE TABLE `ielts` (
  `id` int(10) UNSIGNED NOT NULL,
  `exam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taken_on` date DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ielts`
--

INSERT INTO `ielts` (`id`, `exam`, `user_id`, `score`, `taken_on`, `grade`, `created_at`, `updated_at`) VALUES
(1, 'et', 4, '826', '2003-08-19', 'a', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(2, 'dolorem', 1, '986', '1995-02-07', 'd', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 'dolores', 4, '403', '1981-01-20', 'e', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 'voluptatem', 2, '459', '1971-07-04', 'a', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(5, 'eos', 3, '217', '1976-06-19', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(6, 'dolorem', 2, '143', '1974-08-20', 'c', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(7, 'velit', 3, '898', '2007-12-31', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(8, 'maxime', 1, '141', '1970-08-25', 'e', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(9, 'veniam', 4, '257', '1993-04-22', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(10, 'dolores', 1, '611', '2002-04-12', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(11, 'est', 4, '286', '1990-04-14', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(12, 'impedit', 1, '964', '1973-10-13', 'a', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(13, 'animi', 3, '86', '1987-02-14', 'e', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(14, 'laboriosam', 3, '845', '2002-03-08', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(15, 'quos', 4, '32', '2000-08-15', 'c', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(16, 'non', 4, '465', '1983-01-30', 'b', '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `since` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `name`, `user_id`, `since`, `created_at`, `updated_at`) VALUES
(1, 'assumenda', 3, '1985-07-09', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(2, 'consectetur', 1, '1996-03-13', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 'id', 4, '2016-11-21', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 'soluta', 1, '1981-08-13', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(5, 'amet', 1, '2014-07-04', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(6, 'consequatur', 1, '1992-04-16', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(7, 'repudiandae', 3, '1970-02-09', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(8, 'deserunt', 3, '1995-12-14', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(9, 'nihil', 2, '1974-04-16', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(10, 'tempore', 2, '1979-03-10', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(11, 'rerum', 3, '1982-07-03', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(12, 'similique', 3, '2013-09-09', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(13, 'dolores', 1, '1988-11-12', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(14, 'corrupti', 4, '1973-07-21', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(15, 'deleniti', 2, '2000-11-12', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(16, 'ratione', 2, '2009-07-22', '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(63, '2014_10_12_000000_create_users_table', 1),
(64, '2014_10_12_100000_create_password_resets_table', 1),
(65, '2018_11_15_132208_create_roles_table', 1),
(66, '2018_11_17_121815_create_profile_options_table', 1),
(67, '2018_11_17_121854_create_education_table', 1),
(68, '2018_11_17_122006_create_ielts_table', 1),
(69, '2018_11_17_122037_create_videos_table', 1),
(70, '2018_11_20_081154_create_interests_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_options`
--

CREATE TABLE `profile_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `option_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile_options`
--

INSERT INTO `profile_options` (`id`, `user_id`, `option_key`, `option_value`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'date_of_birth', '2008-01-24', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(2, 1, 'gender', 'Male', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 1, 'country', 'Nicaragua', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 1, 'city', 'Lindgrenport', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(5, 1, 'address', '235 Balistreri Mountain Apt. 351\nTyshawnstad, KY 80964', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(6, 1, 'facebook_url', 'https://facebook.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(7, 1, 'twitter_url', 'https://twitter.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(8, 1, 'instragram_url', 'https://instragram.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(9, 1, 'google_url', 'https://google.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(10, 2, 'date_of_birth', '1970-06-18', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(11, 2, 'gender', 'Male', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(12, 2, 'country', 'Tonga', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(13, 2, 'city', 'North Jaronland', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(14, 2, 'address', '582 Roberto Flat\nReichelfurt, VT 69109-0525', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(15, 2, 'facebook_url', 'https://facebook.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(16, 2, 'twitter_url', 'https://twitter.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(17, 2, 'instragram_url', 'https://instragram.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(18, 2, 'google_url', 'https://google.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(19, 3, 'date_of_birth', '1983-07-24', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(20, 3, 'gender', 'Male', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(21, 3, 'country', 'Jersey', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(22, 3, 'city', 'Port Malvinafort', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(23, 3, 'address', '507 Ellen Harbors\nLake Uriahtown, NM 21456-0293', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(24, 3, 'facebook_url', 'https://facebook.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(25, 3, 'twitter_url', 'https://twitter.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(26, 3, 'instragram_url', 'https://instragram.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(27, 3, 'google_url', 'https://google.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(28, 4, 'date_of_birth', '1973-03-27', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(29, 4, 'gender', 'Male', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(30, 4, 'country', 'Monaco', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(31, 4, 'city', 'East Earline', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(32, 4, 'address', '926 Fahey Hill\nEdgarchester, AK 70758-5149', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(33, 4, 'facebook_url', 'https://facebook.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(34, 4, 'twitter_url', 'https://twitter.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(35, 4, 'instragram_url', 'https://instragram.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(36, 4, 'google_url', 'https://google.com', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(2, 'Student', 'student', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 'School', 'school', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 'Tutor', 'tutor', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(5, 'Counselor', 'counselor', '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(6, 'Parent', 'parent', '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_name`, `role_id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'shibu', 'shibu', 2, 'shibu@gmail.com', NULL, '$2y$10$UK2XVtcnCLKk83JEkCJKYeNHmsI1vDkclzJAuYQe4uK7MiLoef8eC', NULL, '2018-11-20 13:19:51', '2018-11-20 13:19:51'),
(2, 'ridwanul', 'ridwanul', 2, 'ridwanul@gmail.com', NULL, '$2y$10$LP/aevt4Qul2UiQEg9UyYOMkm9kAE9j/rHH9GogT6TTc8qsQoxo4C', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(3, 'palash', 'palash', 2, 'palash@gmail.com', NULL, '$2y$10$ZhC.Z.8T6ofKp3ExhhHMgud0Scrl9DTjuQGriuXvCl8g2l9wKmpX6', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52'),
(4, 'sumon', 'sumon', 1, 'sumon@gmail.com', NULL, '$2y$10$QcDojmz3HefWzYMnbkgLGOKgtqijegwi72wp7FzDQeCDxh2AKUXF2', NULL, '2018-11-20 13:19:52', '2018-11-20 13:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ielts`
--
ALTER TABLE `ielts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profile_options`
--
ALTER TABLE `profile_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_name_unique` (`user_name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ielts`
--
ALTER TABLE `ielts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `profile_options`
--
ALTER TABLE `profile_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
