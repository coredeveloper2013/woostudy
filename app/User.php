<?php

namespace App;

use App\Education;
use App\Ielts;
use App\Interest;
use App\ProfileOption;
use App\Role;
use App\Video;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class User extends Authenticatable
{
    use Notifiable;
    use HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_name', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
      return $this->role->id == 1;
    }
    public function isStudent()
    {
      return $this->role->id == 2;
    }
    public function isSchool()
    {
      return $this->role->id == 3;
    }
    public function isTutor()
    {
      return $this->role->id == 4;
    }
    public function isCounselor()
    {
      return $this->role->id == 5;
    }
    public function isParent()
    {
      return $this->role->id == 6;
    }
    public function getRole()
    {
      return $this->role->name;
    }


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('user_name');
    }

    public function role ()
    {
      return $this->belongsTo(Role::class);
    }

    public function profile_options()
    {
      return $this->hasMany(ProfileOption::class);
    }
    public function educations()
    {
      return $this->hasMany(Education::class);
    }
    public function ielts()
    {
      return $this->hasMany(Ielts::class);
    }
    public function videos()
    {
      return $this->hasMany(Video::class);
    }
    public function interests()
    {
      return $this->hasMany(Interest::class);
    }
    public function addEducation($data) {
      $data['user_id'] = $this->id;
      return $this->educations->create($data);;
    }


}
