<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ParentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::check() && Auth::user()->isParent())
      {
        return $next($request);
      } else {
        return redirect()->route('login');
      }

    }
}
