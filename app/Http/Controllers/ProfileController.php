<?php

namespace App\Http\Controllers;

use App\Education;
use App\Ielts;
use App\Interest;
use App\ProfileOption;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
  public function index($slug)
  {
    $user =  User::where('user_name', $slug)->firstOrFail();
    return view('frontend.profiles.index', compact('user'));
  }
  public function get_auth_user_data()
  {
    return User::with('role')->where('id', auth()->id())->first();
  }
  function get_profile_options($collection, $fields)
  {
    $final_values =[];
    $fields = ['palash', 'sumon'];
    foreach ($fields as $value) {
      $final_values[$value] = '';
    }

    foreach ($users as $user) {
      if (in_array($user->user_name, $fields)) {
        $final_values[$user->user_name] = $user->name;
      }
    }
    return $final_values;
  }

  public function generate_empty_array_value($fields)
  {

    $final_values = [];
    foreach ($fields as $key) {
      $final_values[$key] = '';
    }
    return $final_values;
  }
  public function get_profile_personal_info($profile_id)
  {
    // name, user, gender, date_of_birth
    $user = User::find($profile_id);
    $final_values = [];
    $fields = ['gender', 'date_of_birth'];
    $final_values = $this->get_values_from_options_table_by_fields($fields, $profile_id);
    $final_values['user_name'] = $user->user_name;
    $final_values['name'] = $user->name;
    return $final_values;

  }
  public function post_profile_personal_info()
  {
    $user = auth()->user();
    $date_of_birth = request('date_of_birth');
    $gender = request('gender');
    $name = request('name');
    $user_name = request('user_name');
    $message = [];
    $might_different_user = User::where('user_name', $user_name)->first();

    if ($might_different_user) {
      if ($might_different_user->id != $user->id){
        $message['error'] = 'user_name all ready taken exists';
        return $message;
      }
    }

    $user->name = $name;
    $user->user_name = $user_name;
    $user->save();
    $this->set_or_update_profile_options('date_of_birth', $date_of_birth);
    $this->set_or_update_profile_options('gender', $gender);
    $message['success'] = 'Profile updated successfully';
    return $message;
  }

  public function set_or_update_profile_options($key, $value) {

    $user_id = auth()->id();
    $option = ProfileOption::where('option_key', $key)->where('user_id', $user_id)->first();
    if ($option) {
      $option->option_value = $value;
      $option->save();
    } else {
      ProfileOption::create([
        'user_id' => $user_id,
        'option_key' => $key,
        'option_value' => $value,
      ]);
    }
  }

  public function set_or_update_profile_options_by_fields($fields)
  {
    $option_fields = [];
    foreach ($fields as $value) {
      $option_fields[$value] = request($value);
    }
    foreach ($option_fields as $key => $value) {
      $this->set_or_update_profile_options($key, $value);
    }
    return true;
  }


  public function get_values_from_options_table_by_fields($fields, $id) {
    $final_values = [];
    $user = User::find($id);
    $options = ProfileOption::where('user_id', $user->id)->get();
    $final_values = $this->generate_empty_array_value($fields);
    foreach ($options as $option) {
      if(in_array($option->option_key, $fields)) {
        $final_values[$option->option_key] = $option->option_value;
      }
    }
    return $final_values;
  }

  public function get_contact_details($id)
  {
    $fields = ['country', 'city', 'address'];
    return $this->get_values_from_options_table_by_fields($fields, $id);
  }


  public function get_social_media_details($id)
  {
    $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
    return $this->get_values_from_options_table_by_fields($fields, $id);
  }
  public function post_contact_details()
  {
    $fields = ['country', 'city', 'address'];
    $this->set_or_update_profile_options_by_fields($fields);
    return response()->json([
      'success' => 'update information successfully'
    ]);

  }
  public function post_social_media_details()
  {
    $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
    $this->set_or_update_profile_options_by_fields($fields);
    return response()->json([
      'success' => 'update information successfully'
    ]);
  }


  /**
   * following 4 functions for education component
   */

  public function get_education_details($id)
  {
    $user = User::find($id);
    return Education::where('user_id', $user->id)->get();
  }
  public function post_education_details()
  {
    $fields = ['grade_name', 'grade', 'institute', 'completion_date', 'percentage_obtained', 'education_level'];
    $data = [];
    foreach ($fields as $field) {
      $data[$field] = request($field);
    }
    $data['user_id'] = auth()->id();
    return Education::create($data);
  }
  public function delete_education_item($id)
  {
    $education = Education::find($id);
    $education->delete();
    return response()->json(['success' => 'deleted successfully']);
  }
  public function update_education_item(Request $request, $id)
  {
    $education = Education::find($id);
    $education->grade_name = request('grade_name');
    $education->grade = request('grade');
    $education->institute = request('institute');
    $education->completion_date = request('completion_date');
    $education->percentage_obtained = request('percentage_obtained');
    $education->education_level  = request('education_level');
    $education->save();

    return response()->json([
      'success' => 'Education Details update successfully'
    ]);
  }


  // itelts details

  public function get_ielts_details($id)
  {
    $user = User::find($id);
    return Ielts::where('user_id', $user->id)->get();
  }
  public function post_ielts_details()
  {
    $fields = ["exam", "score", "taken_on", "grade"];
    $data = [];
    foreach ($fields as $field) {
      $data[$field] = request($field);
    }
    $data['user_id'] = auth()->id();
    return Ielts::create($data);

  }
  public function delete_ielts_item($id)
  {
    $ielts = Ielts::find($id);
    $ielts->delete();
    return response()->json(['success' => 'deleted successfully']);
  }
  public function update_ielts_item(Request $request, $id)
  {
    $fields = ["exam", "score", "taken_on", "grade"];
    $data = [];
    foreach ($fields as $field) {
      $data[$field] = request($field);
    }
    Ielts::where('id', $id)->update($data);
      return response()->json([
        'success' => 'Ielts Details update successfully',
      ]);
  }


  // interest page

  public function get_interest_details($id)
  {
    $user = User::find($id);
    return Interest::where('user_id', $user->id)->get();
  }
  public function post_interest_details()
  {

    $fields = ["name", "since"];
    $data = [];
    foreach ($fields as $field) {
      $data[$field] = request($field);
    }
    $data['user_id'] = auth()->id();
    return Interest::create($data);

  }
  public function delete_interest_item($id)
  {

    $ielts = Interest::find($id);
    $ielts->delete();
    return response()->json(['success' => 'deleted successfully']);

  }
  public function update_interest_item($id)
  {

    $fields = ["name", "since"];
    $data = [];
    foreach ($fields as $field) {
      $data[$field] = request($field);
    }
    Interest::where('id', $id)->update($data);
    return response()->json([
      'success' => 'Ielts Details update successfully',
    ]);
  }


  public function post_video_details(Request $request)
  {
      $uploadedFile = $request->file('file');

      $without_extension = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME); // getting only file name without extension to make it slugify
      $extension = $uploadedFile->getClientOriginalExtension();
      $without_extension =  str_slug($without_extension); // make slugify only name
      $filename = time().$without_extension.'.'.$extension; // time() + name + . + extension
      Storage::disk('local')->putFileAs(
        'public/uploads/',
        $uploadedFile,
        $filename
      );

      // putFileAs take 3 parameters. 1.folder, 2.file 3.my_desired_filename


      Video::create([
        'title' => $request->title,
        'video' => $filename,
        'user_id' => auth()->id(),
      ]); // adding info to the database

      return response()->json([
        'success' => 'Video Details update successfully',
      ]);

  }

  public function get_video_details($id)
  {
    $user = User::find($id);
    return Video::where('user_id', $user->id)->get();
  }
  public function delete_video_item($id)
  {
    $video = Video::find($id);
    $file_path = 'uploads/'.$video->video;
    $ds = Storage::delete($file_path); // currently its not deleting
    $video->delete();
    return response()->json([
      'success' => 'Video Deleted successfully',
      'status' => $ds,
    ]);
  }




}
