<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Video extends Model
{
  protected $guarded = [];

  protected $appends = ['video_url'];
  public function getVideoUrlAttribute() {
    return url( Storage::url('uploads/'.$this->video));
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
