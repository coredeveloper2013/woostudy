<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
  protected $guarded = [];
  public function users()
  {
    return $this->belongsTo(User::class);
  }
}
