<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'addresses', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'country_id' );
            $table->unsignedInteger( 'state_id' )->nullable();
            $table->string( 'city' );
            $table->string( 'address' )->nullable();
            $table->string( 'lat' )->nullable();
            $table->string( 'lng' )->nullable();
            $table->string( 'addressable_id' );
            $table->string( 'addressable_type' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'addresses' );
    }
}
