<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'countries', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'region_id' );
            $table->string( 'title' )->unique()->index();
            $table->string( 'code' )->unique()->index();
            $table->string( 'slug' )->unique();
            $table->string( 'flag' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'countries' );
    }
}
