<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'invitations', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'invited_from' );
            $table->unsignedInteger( 'invite_to' );
            $table->unsignedInteger( 'invitable_id' );
            $table->string( 'invitable_type' );
            $table->tinyInteger( 'status' )->default( 2 )->index();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'invitations' );
    }
}
