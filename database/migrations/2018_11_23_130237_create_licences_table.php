<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'licences', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'user_id' );
            $table->string( 'title' );
            $table->string( 'authority' )->nullable();
            $table->date( 'start_date' );
            $table->date( 'valid_till' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'licences' );
    }
}
