<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'experiences', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'institute_id' );
            $table->unsignedInteger( 'user_id' );
            $table->date( 'from_date' );
            $table->date( 'to_date' )->nullable();
            $table->boolean( 'continue' )->default( false );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'experiences' );
    }
}
