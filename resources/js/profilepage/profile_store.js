// adding store for future add
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    roles: ['admin', 'students', 'tutor'],
    authUser: {},
    profile_id: null,
  },
  getters: {
    roles (state) {
      return state.roles;
    },
    authUser(state) {
      return state.authUser;
    },
    profile_id(state) {
      return state.profile_id;
    },
    auth_user_id(state) {
      return state.authUser.id;
    }

  },
  mutations: {
    addRoles(state, newRole) {
      return state.roles = [...state.roles, newRole]
    },
    setAuthUserData(state, data) {
      return state.authUser = data
    },
    set_profile_id(state, id) {
      return state.profile_id = id;
    }
  },
  actions: {

  }
})
