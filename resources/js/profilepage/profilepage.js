/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import 'vue2-dropzone/dist/vue2Dropzone.css'
import {store} from './profile_store.js'
import VModal from 'vue-js-modal'

window.Vue = require('vue');
window.ProfileEvent = new Vue();
Vue.use(VModal);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


// generic
//

Vue.component('g-alert', require('./components/generic/Alert.vue'));
Vue.component('g-modal', require('./components/generic/Modal.vue'));

Vue.component('app-test', require('./components/Test.vue'));

Vue.component('app-init', require('./components/Init.vue'));

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('app-profile', require('./components/profiles/Profile.vue'));

// profile components
Vue.component('profile-sidebar', require('./components/profiles/ProfileSidebar.vue'));
Vue.component('profile-personal', require('./components/profiles/PersonalInformation.vue'));
Vue.component('profile-contact', require('./components/profiles/ContactDetails.vue'));
Vue.component('profile-education', require('./components/profiles/EducationDetails.vue'));
Vue.component('profile-ielts', require('./components/profiles/IeltsTofel.vue'));
Vue.component('profile-interest', require('./components/profiles/Interest.vue'));
Vue.component('profile-video', require('./components/profiles/VideoBlock.vue'));

//finds info
Vue.component('find-institute', require('./components/finds/FindInstitute'));
Vue.component('find-invitation', require('./components/finds/FindInvitation'));
Vue.component('find-tutor', require('./components/finds/FindTutor'));
Vue.component('find-mutual-friend', require('./components/finds/FindMutualFriend'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
